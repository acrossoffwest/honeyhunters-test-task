<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment.firstname' => 'required',
            'comment.email' => 'required|email',
            'comment.message' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'comment.firstname.required' => 'Обязательное поле',
            'comment.email.required'  => 'Обязательное поле',
            'comment.email.email'  => 'Введите правильный email',
            'comment.message.required'  => 'Обязательное поле',
        ];
    }
}
