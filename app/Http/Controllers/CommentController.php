<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Model\Comment;
use Illuminate\Support\Facades\Input;

class CommentController extends Controller
{
    public function create (CommentRequest $request)
    {
        $comment = Input::get('comment');

        $model = new Comment($comment);
        $model->save();
        
        return response()->json([
            'success' => 1,
            'data' => $model->toArray()
        ]);
    }
}
