<?php

namespace App\Http\Controllers;

use App\Model\Comment;

class HomeController extends Controller
{
    public function index ()
    {
        $title = 'Test task';
        $comments = Comment::all();

        return view('section.home.index', compact('title', 'comments'));
    }
}
