## Test task for HoneyHunters

> Backend:
>> Framework: Laravel 5.4
>>> Backpack: https://github.com/laravel-backpack

> Frontend:
>> JS:
>>> Webpack
>>> Underscore
>>> jQuery
>>> VueJS
>>> Bootstrap

>> CSS(SCSS):
>>> Bootstrap
