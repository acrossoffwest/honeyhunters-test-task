window.$ = window.jQuery = require('jquery');
const Vue = require('vue');
window._ = require('underscore');
require('bootstrap');

Vue.component('vue-comment-item', {
    template: '#vue-comment-item',
    props: ['model']
});

$(function () {
    let app = new Vue({
        el: '#app',
        data: {
            comments: comments
        }
    });
    let commentForm = $('#comment-form');

    commentForm.on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            type: 'post',
            url: route('comment.create.post'),
            data: commentForm.serialize(),
            dataType: 'json',
            success: function(response){
                if (response && !response.success) {
                  return alert('Test');
                }
                if (!_.isArray(app.comments)) {
                    app.comments = [];
                }
                app.comments.push(response.data);
                $("input[type=text], textarea").val("");
            },
            error: function(data){
                var errors = data.responseJSON;
                $('.form-group .error').remove();
                $.each(errors, function (index, item) {
                    index = index.replace('.', '_');
                    $('#' + index)
                        .closest('.form-group')
                        .append(
                            '<label id="' + index + '-error" class="error" for="' + index + '">' + item + '</label>'
                        );
                });
            }
        });
    });
});
