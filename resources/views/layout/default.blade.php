<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title') | {{ env('APP_PROJECT_NAME') }}</title>
        <link rel="stylesheet" href="/css/app.css">
        @yield('style')
    </head>
    <body>
        @yield('content')
        @yield('script-vars')
        <script type="text/javascript" src="/js/app.js"></script>
        @yield('script')
    </body>
</html>
