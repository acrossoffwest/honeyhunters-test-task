@extends('layout.default')
@section('title', $title)
@section('content')
    <div  id="app">
        <div class="gray-back">
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-sm-4">
                            <div class="logo">
                                <img src="/img/logo.png" alt="Logo">
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section class="first">
                <div class="container">
                    <form id="comment-form" action="{{ route('comment.create.post') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-offset-4 col-sm-3 col-xs-offset-1 col-xs-10">
                                <div class="contact-icon-wrapper">
                                    <img src="/img/contact-icon.png" alt="Contact Icon">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-5">
                                <div class="form-group">
                                    <label for="comment_firstname">Имя <span class="required">*</span></label>
                                    <input class="form-control" type="text" name="comment[firstname]" value="" id="comment_firstname">
                                </div>
                                <div class="form-group">
                                    <label for="comment_email">E-mail <span class="required">*</span></label>
                                    <input class="form-control" type="text" name="comment[email]" value="" id="comment_email">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-offset-1  col-sm-6">
                                <div class="form-group">
                                    <label for="comment_message">Комментарий <span class="required">*</span></label>
                                    <textarea class="form-control" name="comment[message]" rows="8" cols="80" id="comment_message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-9 col-sm-3">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-comment-send" type="button" name="button">Записать</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        <section class="second">
            <div class="container">
                <h2>Выводим комментарии</h2>
                <div class="row" v-if="comments">
                    <vue-comment-item v-for="comment in comments" :model="comment"></vue-comment-item>
                </div>
            </div>
        </section>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 col-sm-3">
                        <div class="logo">
                            <img src="/img/logo.png" alt="Logo">
                        </div>
                    </div>
                    <div class="col-sm-offset-7 col-sm-1 col-xs-offset-2 col-xs-2 social-link-wrapper">
                        <a href="#">
                            <img src="/img/icon-facebook.png" alt="">
                        </a>
                    </div>
                    <div class="col-sm-1 col-xs-2 social-link-wrapper">
                        <a href="#">
                            <img src="/img/icon-vk.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <template id="vue-comment-item">
        <div class="col-sm-4 comment">
            <div class="header">
                @{{ model.firstname }}
            </div>
            <div class="body">
                <div class="email">
                    @{{ model.email }}
                </div>
                <div class="message">
                    @{{ model.message }}
                </div>
            </div>
        </div>
    </template>
@stop

@section ('script-vars')
    <script type="text/javascript">
        window.route = function (routeName) {
            const Routes = {
                'comment.create.post': '{{ route('comment.create.post') }}'
            };
            let url = Routes[routeName] ? Routes[routeName] : '';
            return Routes[routeName];
        };
        let comments = {!! json_encode($comments->values()->toArray()) !!};
    </script>
@stop
@section ('script')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
@stop
